# ASPNETCore.HowTos.Healthchecks

**To read**: <https://codingstories.io/story/https:%2F%2Fgitlab.com%2Fcodingstories%2Fhow-to-add-health-checks-to-aspnetcore>

**Estimated reading time**: 10 min

## Story Outline

The story explains you how to add various health checks to ASP.NET Core project.

## Story Organization

**Story Branch**: main
> `git checkout main`

Tags: #csharp, #aspnetcore, #healthchecks
