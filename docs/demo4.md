# Demo 3 - RabbitMQ

Let's shut down the `mockserver` instance and see what happens.

`https://localhost:5001/> GET health`

```http
HTTP/1.1 503 Service Unavailable
Cache-Control: no-store, no-cache
Content-Type: text/plain
Date: Sun, 25 Jul 2021 13:57:47 GMT
Expires: Thu, 01 Jan 1970 00:00:00 GMT
Pragma: no-cache
Server: Kestrel
Transfer-Encoding: chunked

Unhealthy
```

And when the `mockserver` instance is available:

`https://localhost:5001/> GET health`

```htttp
HTTP/1.1 200 OK
Cache-Control: no-store, no-cache
Content-Type: text/plain
Date: Sun, 25 Jul 2021 13:58:06 GMT
Expires: Thu, 01 Jan 1970 00:00:00 GMT
Pragma: no-cache
Server: Kestrel
Transfer-Encoding: chunked

Healthy
```
