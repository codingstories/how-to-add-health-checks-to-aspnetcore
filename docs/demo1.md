# Demo 1 - basic probe

Let's use [HTTP REPL](https://docs.microsoft.com/en-us/aspnet/core/web-api/http-repl/) to test basic health check endpoint.

```bash
$ httprepl.exe https://localhost:5001/

https://localhost:5001/> GET health
```

Result:

```http
HTTP/1.1 200 OK
Cache-Control: no-store, no-cache
Content-Type: text/plain
Date: Sun, 25 Jul 2021 10:58:22 GMT
Expires: Thu, 01 Jan 1970 00:00:00 GMT
Pragma: no-cache
Server: Kestrel
Transfer-Encoding: chunked

Healthy


```
